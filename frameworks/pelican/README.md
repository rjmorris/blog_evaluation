# Pelican

I decided to try Pelican because it's the most popular static-site generator written in my most comfortable language (Python).

## Quickstart

Below are the quickstart instructions from the website at the time I tested it, along with my comments on each step.

```
$ python -m pip install "pelican[markdown]"
```

Instead of this, I created a virtual environment and added `pelican[markdown]` to a requirements file. Pelican 4.5.0 was installed.

```
$ mkdir -p ~/projects/yoursite
$ cd ~/projects/yoursite
```

In my case, I'd already created a directory to hold my requirements file and was already in it, so I didn't need this step.

```
$ pelican-quickstart
```

This runs through a setup wizard for scaffolding the project. It seemed reasonably straightforward, although I had a couple of complaints:

- I didn't understand the question "Do you want to enable article pagination?" This detail seems pretty low-level for a setup wizard.
- It asked whether to "generate a tasks.py/Makefile to automate generation and publishing" with a default of yes, which I accepted, but I found the resulting files a bit confusing.
    - They include lots of targets that seem subtly different but without much explanation. I probably wouldn't use these and would instead write something simpler for myself.
    - `Makefile` and `tasks.py` seem to have duplicate functionality, so it isn't clear why both were created. Maybe the setup wizard should ask which one to create.
    - `tasks.py` requires the Python package `invoke`, but that package isn't installed.

```
Create a markdown file inside content/ that looks something like:

Title: My First Review
Date: 2010-12-03 10:20
Category: Review

Following is a review of my favorite mechanical keyboard.
```

I copied `/posts/text_only/index.md` from this repo into the `contents/` directory, renamed it to `text_only.md`, and added a metadata header.

```
$ pelican content
```

This generated the site. The instructions noted that there may be a warning related to feeds. I didn't get that warning, but I did get one about a watched path not existing (`content/images`), which wasn't mentioned in the instructions.

The generated files were created in the `output/` directory. I saw that my `text_only.md` file was renamed to match the "title" metadata field (`short-post-containing-only-text.html` in my case). I'd prefer the generated file names to match the source file names. I also saw files and directories for authors, categories, and tags, none of which I want.

```
$ pelican --listen
```

This started a dev server, allowing me to view the site in my browser. I'm picky about my ports, so I used `pelican --help` to find the option for changing the port and ran `pelican --listen --port 8600` instead.

Upon viewing the site, it was nice to see that a theme was included by default, although of course I'll be changing it later. Aside from the cosmetics, it includes things I don't want, such as categories, links to other sites (referred to in the `pelicanconf.py` as a "blogroll"), and a place to add social links.

Overall, the quickstart went smoothly and quickly, although I'll be looking to change several settings and behaviors if I move forward with Pelican.
