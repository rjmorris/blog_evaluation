# Hugo

## Quickstart

Below are the quickstart instructions from the website at the time I tested it, along with my comments on each step.

The installation section mentioned only macOS with a link to a separate page for Linux instructions. The Linux method is to download and extract the binary. I downloaded `hugo_0.78.0_Linux-64bit.tar.gz` and extracted it to the current directory. This installation method seems great to me, although I think they should have mentioned it on the quickstart page.

> `hugo new site quickstart`

This creates a new site in the directory named `quickstart`. I wanted to create it in the current directory, so I tried `hugo new site .`, but you have to pass it the `--force` flag to create a site in a nonempty directory, which I ended up doing.

It didn't ask me any questions, just created the skeleton. It also provided some instructions for next steps, which were essentially the following quickstart steps.

> Add a theme by downloading one from GitHub:
>
> `cd quickstart`
> `git init`
> `git submodule add https://github.com/budparr/gohugo-theme-ananke.git themes/ananke`

I skipped the first command since I created the site in the current directory, and I skipped the second command because I'm already working in a git repo.

The code blocks in the documentation are annoying when they contain long lines that need scrolling, such as the one above. When you mouse over it, the width of the code block jumps wide enough to show the whole line. Aside from the annoying jumpting, this means that you can't actually scroll the code block. You have to scroll the whole page instead.

> Then, add the theme to the site configuration:
>
> `echo 'theme = "ananke"' >> config.toml`

One minor suggestion would be to go ahead and set this up as part of the `quickstart` command.

> Add content by manually creating a file like `content/<CATEGORY>/<FILE>.<FORMAT>` or use the `new` command:
>
> `hugo new posts/my-first-post.md`

I'm much more likely to create the files directly than use the `new` command, but I tried the `new` command. It created a blank post with pre-populated frontmatter containing the title (gleaned from the file name), date/time, and setting the `draft` flag. The quickstart instructions don't say what format the frontmatter should be in, but it appears to be YAML.

I deleted the auto-generated file and copied `/posts/text_only/index.md` from this repo into the `content/posts/` directory, renamed it to `text_only.md`, and added the frontmatter.

I'm not sure whether the full timestamp is really necessary. I doubt I'd need anything more precise than the date. I decided to try removing the time part and keeping just the date.

I omitted the `draft` flag, because I intend to handle drafts by writing them in a separate git branch.

I'd rather not be required to put the files inside a category subdirectory. I'd rather put them directly inside `content/`.

> Start the Hugo server with `hugo server -D`

The `-D` option enables drafts, but I omitted that because I disabled the draft status on my post. And I like to specify the port, so I found that option in `./hugo server --help`. I ran `./hugo server --port 8601`.

The suggested theme is pretty clean and what I would consider a good starting point for a quickstart. It displays a generic title ("My New Hugo Site") that will need to be updated. Using just a date instead of date+time in the sample post's metadata appears to work just fine.

The site is automatically updated and reloaded when I edit the content, which is a plus during development.

> Customize the theme by editing `config.toml`

The default config file is refreshingly short and straightforward. It includes only the following settings:

- `baseURL`
- `languageCode`
- `title`
- `theme` (which I added earlier during the quickstart)

I changed the base URL and title.

> Build static pages with `hugo -D`

The output goes to `./public`. The quickstart instructions provide two easy ways to change the destination, but the default seems fine to me.

My `text_only.md` file was changed into a directory named `text_only` with an `index.html` inside. I saw directories for categories and tags, neither of which I want, plus some things like a sitemap and pagination, which I guess is fine but I could do without.
