# Zola

I decided to try Zola because it appeared to have a lot of features built-in, like Hugo, but with a more familiar template engine than Hugo.

## Quickstart

Zola's documentation doesn't have a section described as a "quickstart". Below are the instructions from the Getting Started: Overview page, which seems similar in spirit, along with my comments on each step.

To install, the Getting Started page referred me to a different Installation page, which mentioned that pre-built binaries are available from the GitHub release page. From there, I downloaded `zola-v0.12.2-x86_64-unknown-linux-gnu.tar.gz` and extracted it to the current directory. The instructions say they're based on version 0.9, so hopefully they aren't too outdated.

> Initialize the site with `zola init myblog`.

This creates a new site in the directory named `myblog`. I wanted to create it in the current directory, so I tried `zola init .`, but that reported an error saying the current directory isn't empty. From `zola init --help` I found the `-f` flag to create a project in a non-empty directory, which I ended up using.

The init process asked a few questions, and I accepted the default for all but the site URL. I thought the questions seemed a little too in the weeds for this stage: enabling Sass compilation, enabling syntax highlighting, and building a search index. It said my choices could be changed later by modifying `config.toml`.

> Start the development server with `zola serve`

I like to specify the port, and from `zola serve --help`, I found that I can use the `--port` option. So I ran `zola serve --port 8603`.

I haven't added any content to the site, but after loading it in my browser, I get a "Welcome to Zola" page, which seems like a good way to handle the initial state.

> Make a home page by first creating `base.html` inside the `templates` directory.

I copied the code snippet from the instructions page. It sets up the `<html>`, `<head>`, and `<body>` tags that will wrap every page on the site, and it includes a placeholder for the page content.

> Next create an `index.html` file inside the `templates` directory.

Again, I copied the code snippet from the instructions page. It extends `base.html` and inserts an `h1` containing generic text into the content placeholder.

After saving this file, the server detected the change and auto-reloaded the page.

> Add content by creating `_index.md` in the `content/blog` directory.

The `blog` directory represents a "section" in Zola terminology, and the section will have its own index page. The Getting Started guide assumes that the overall site has one index page (`templates/index.html` currently), and then the blog section has its own index page displaying the list of blog posts (`contents/blog/_index.md`). This structure is more complicated than I intend to use for my own blog site, where the overall index page will display the list of blog posts, and I won't have sections. However, I'll go with it for now just to follow the instructions.

I copied the code snippet from the instructions page. It adds TOML frontmatter that defines the page title, a sort-by field, a template (`blog.html`), and a page template (`blog-page.html`). The distinction between `template` and `page_template` is that `template` is used for the `_index.md` in this section, and `page_template` is used for individual content pages in this section.

> Create `templates/blog.html`.

Again, I copied the code snippet from the instructions page. It extends `base.html` and populates the content placeholder with a list containing links to all the pages in the section. (We haven't currently created any pages in the section, so the list is empty when viewing it in the browser.)

> Create a post in the `content/blog` directory.

Here I used one of my own sample posts instead of copying the the code snippet from the instructions. I copied `/posts/text_only/index.md` from this repo into the `content/blog/` directory, renamed it to `text_only.md`, removed the header containing the title (since it will be in the frontmatter), and added the frontmatter.

After saving this file, the server printed an error saying it couldn't render the page. The instructions don't mention that, but I suspect it's because I haven't created the `blog-page.html` template yet.

> Create `templates/blog-page.html`.

Again, I copied the code snippet from the instructions page. It extends `base.html` and populates the content placeholder with markup displaying the post's title, date, and text.

Upon saving, the server said it reloaded only templates, and the blog index page in the browser didn't reload to show my post. The instructions said it would. I went back to `content/blog/text_only.md` and made a trivial change. This time the server rendered it and reloaded the blog index page in the browser.

> Create a second post in the `content/blog` directory.

I decided to skip this step. Nothing special will happen with two posts compared to one, and having just one post is consistent with the quickstarts for the other frameworks.

> Modify the home page to link to the blog posts.

Again, I copied the code snippet from the instructions page. It edits `templates/index.html` to add a link to `/blog/`.

The getting started guide doesn't say how to generate static pages, and the server doesn't create them for me. From `zola --help`, I found that `zola build` is the command to generate the static pages. The output ends up in `./public`. Inspecting that directory, I see that my `text_only.md` file was changed into a directory named `text-only` with an `index.html` inside. There isn't too much additional stuff in there, though: only a 404 page, `robots.txt`, and `sitemap.xml`.
