# Eleventy (11ty)

I decided to try eleventy because of its simple philosophy. It compiles all template files in the current directory into the output directory, mirroring the directory structure. There are no restrictions on what goes

## Quickstart

Below are the quickstart instructions from the website at the time I tested it, along with my comments on each step.

> Eleventy v0.11.1 requires Node 8 or newer.

I added `PATH_add ~/apps/opt/node/12.19.0/bin` to `./.envrc` so that direnv will add an appropriate version of Node to my PATH upon entering this directory.

> `npm install -g @11ty/eleventy`

I didn't install eleventy globally. Instead, I created a local npm package and installed it there. First I created a barebones `package.json` with contents:

```
{
  "name": "blog-11ty"
}
```

and then I installed eleventy with `npm install @11ty/eleventy`.

> `echo '# Page header' > README.md`

This sample content makes for a _really_ quick quickstart, but I decided to use my standard sample post instead. I copied `/posts/text_only/index.md` from this repo into `./posts/text_only/index.md`.

> `eleventy`

This step builds the site, storing it in `_site/`.

Because I didn't install eleventy globally, I had to modify the command to `npx eleventy`. On second thought, I decided to add `PATH_add ./node_modules/.bin` to `./.envrc` so that `eleventy` would be in my PATH. This allowed me to drop `npx` from the command.

The built site differed from my expectations in a couple of ways:

- This `README.md` file was included. This shouldn't have surprised me, because I was drawn to eleventy by its design of simply compiling all the files in the current directory. This behavior is as advertised. I think I'll want to have some administrative-type files like this one in the real repo, so I'll need to look into either excluding some files from being compiled, or putting the site contents into a separate directory and only compiling those.
- This `README.md` wasn't compiled to `README.html`. Instead, it was compiled to `README/index.html`. I didn't expect eleventy to change the directory structure. My sample post in `posts/text_only/index.md` was compiled to `posts/text_only/index.html`, as expected, so I guess it creates directories for anything named other than `index.md`. My plan for the real repo was to name my posts `index.md` inside a dedicated directory anyway, so this wouldn't be an issue, but it's still good to understand the behavior better.

> Run `eleventy --serve` to start a web server and open http://localhost:8080/README/.

I prefer to specify the port, so I found the `--port` option from `eleventy --help` and ran `eleventy --serve --port 8602`. I first tried to access http://localhost:8602 in my browser, but I was met with the error message "Cannot GET /". That made sense, because I didn't create an `./index.html` file. The instructions did say to access http://localhost:8080/README/, which worked.

The default site has no theme, just bare HTML. This fits with the design philosophy of eleventy, so the bare HTML meets my expectations.

The `--serve` option is supposed to watch files for changes, compiling them and reloading the browser page after each change. I found that it did recompile on changes, but I had to reload the browser manually. I found a bug report about this, and the "Getting Started" guide, as opposed to the "Quick Start" that I followed, mentions that you must have a `<body>` tag in your template for live-reload to work properly. The HTML files compiled from my Markdown files don't include `<body>` or `<head>` or `<html>` tags. I'll need to create a "layout" to get those.

Reviewing the Getting Started guide, I believe that would have been a better starting point for me. It describes how to install eleventy locally instead of globally like the Quick Start guide did. (It even says installing locally is preferred, so why did the Quick Start guide suggest installing globally?)
