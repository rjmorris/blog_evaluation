# Hexo

I decided to try Hexo because it seemed similar to Hugo in terms of features but written in Javascript and with a more familiar templating language.

## Quickstart

Below are the quickstart instructions from the website at the time I tested it, along with my comments on each step.

> Install Hexo with `npm install hexo-cli -g`.

I didn't install Hexo globally. Instead, I created a local npm package and installed it there. First, I created a `~/.envrc` to specify which version of Node I want:

```
PATH_add ~/apps/opt/node/15.0.1/bin
```

Then, I created a barebones `package.json` with contents:

```
{
  "name": "blog-hexo"
}
```

Then I installed Hexo with `npm install hexo-cli`. It installed version 4.2.0.

To run Hexo with just `hexo` instead of `npx hexo`, I added the `node_modules/.bin` directory to my PATH by adding the following line to `~/.envrc`:

```
PATH_add ./node_modules/.bin
```

> Set up your blog with `hexo init blog` and `cd blog`.

I want to put everything in the current directory instead of a subdirectory, so I tried `hexo init .` instead. This gave me an error that the target directory isn't empty. `hexo --help` pointed me to `hexo help init`, where I didn't see an option to force an install into the current directory. However, I did see the `--no-clone` and `--no-install` options, which copy files instead of cloning from GitHub and skip running `npm install`. If it's cloning from GitHub and running `npm install`, then yes, I'd want it in a subdirectory, because otherwise the blog's `.git` and `node_modules` directories would conflict with my own. But why does it need to do that in the first place? Just from a potential confusion standpoint, I don't like the idea of having a git repo and npm package nested under my own git repo and npm package. Regardless, I ran `hexo init blog` and `cd blog` just to move on.

> Start the server with `hexo server`

I like to specify the port, so I found that option in `hexo help server` and ran `hexo server --port 8604`.

Viewing the site in my browser, there is already a theme in place with a default first post that contains essentially a copy of the Quick Start instructions. There are some things I'll want to remove like a search feature and a sidebar with sections for archives and recent posts.

Incidentally, the help text shows a lot more subcommands available when running `hexo --help` inside the `blog` directory. This is no doubt because of the two separate npm packages in the containing directory and the `blog` directory. The binary in the containing directory should probably have been named `hexo-cli` to avoid confusion.

> Create a new post with `hexo new "Hello Hexo"`

I'd rather create the files directly than use the `new` command, but I tried the `new` command. The instructions didn't say where to find the created file, but the output of the command did point to `./source/_posts/Hello-Hexo.md`. This file included pre-populated frontmatter containing the title, date/time, and an empty `tags` field. The instructions don't say what format the frontmatter should be in, but it appears to be YAML.

I deleted the auto-generated file and copied `/posts/text_only/index.md` from this repo into the `source/_posts/` directory, renamed it to `text_only.md`, removed the header containing the title (since it will be in the frontmatter), and added the frontmatter. I also deleted the default `hello-world.md` file that had been generated there during the `init` command.

I'm not sure whether the full timestamp is really necessary. I doubt I'd need anything more precise than the date. I decided to try omitting the time part and using a date only.

I omitted the `tags` field, because I don't intend to use tags and want to see what will happen without it.

I'd rather not be required to put the files inside a category subdirectory. I'd rather put them directly inside `source/`.

After adding the new post and removing the original one, the server didn't automatically update the site for me. The instructions don't say that it should, but I expected it to anyway. After quitting and restarting the server, I could see my post.

> Generate static files with `hexo generate`

The output ends up in `./public`. Inspecting that directory, I see that my `text_only.md` file was rendered to `./public/2020/01/01/text_only/index.html`. There isn't too much additional stuff in there, though: an archives directory with directory structure `archives/2020/01` and an `index.html` at each level; and some JS and CSS that I assume came in from the default theme.
