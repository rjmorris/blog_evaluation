# Post containing multiple images

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet cursus sit amet dictum. Ultrices eros in cursus turpis massa tincidunt dui ut.

## Chart 1

Scelerisque fermentum dui faucibus in ornare quam viverra orci. Scelerisque purus semper eget duis at tellus at urna.

![chart #1](./chart1.png)

In ornare quam viverra orci sagittis eu. Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus.

## Chart 2

Pellentesque nec nam aliquam sem et tortor consequat. Viverra aliquet eget sit amet tellus cras adipiscing.

![chart #2](./chart2.png)

Nam at lectus urna duis convallis convallis. Lorem dolor sed viverra ipsum nunc aliquet. Ultrices mi tempus imperdiet nulla malesuada. Amet mauris commodo quis imperdiet massa. Nibh ipsum consequat nisl vel pretium lectus quam.

## Chart 3

Tincidunt vitae semper quis lectus nulla. Vitae elementum curabitur vitae nunc sed velit. Mattis ullamcorper velit sed ullamcorper morbi tincidunt. Aliquet lectus proin nibh nisl condimentum. Sagittis purus sit amet volutpat consequat. Velit laoreet id donec ultrices.

![chart #3](./chart3.png)

Velit scelerisque in dictum non consectetur a erat nam at. Egestas maecenas pharetra convallis posuere morbi leo urna. Viverra vitae congue eu consequat ac felis donec et odio.
